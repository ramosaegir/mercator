# Mercator

Primeiro projeto fruto de estudos da IDE Android Studio com a tecnologia Java empregada na linguagem de programação.

## Introdução
O aplicativo Mercator parte do princípio de que qualquer consumidor possa ter acesso às suas promoções de uma forma prática e ágil. Logo, a Mercator traz consigo o meio de obter os melhores preços de mercados, drogarias e hortifrutis com comodidade e sem precisar enviar uma mensagem ou ir atrás de um encarte físico. O usuário poderá fazer diversas buscas relacionadas à produtos, municípios, estabelecimentos e por localização de GPS, tudo de maneira intuitiva e fácil de manusear. Além das buscas, no painel do estabelecimento poderá ser acessado seus dados como datas e horários de funcionamento, SAC, descrições, entre outras informações pertinentes.

## Motivação
O projeto surgiu com o objetivo de trazer ao consumidor destes estabelecimentos o comodismo e a praticidade de acessar promoções atualizadas e consequentemente levar economia para o consumidor.

## Interface da Aplicação
![alt text](https://firebasestorage.googleapis.com/v0/b/gitrep-259bc.appspot.com/o/ui_mercator.png?alt=media&token=b2ab243c-dc06-47ef-a2b0-f5fa2324473c)

## Modo de Utilizar
1. Faça seu login ou cadastre-se com os dados requeridos a partir da tela inicial;
2. Ao acessar o painel principal, acesse as promoções especiais de cada dia e localidade, ou faça uma busca conforme sua necessidade;
2.1 Caso optar por ver as promoções da localidade, escolha um dos estabelecimentos contidos e acesse diretamente o painel do estabelecimento para acessar suas promoções, bem como informações úteis da localidade;
2.2 Caso optou por iniciar uma busca, escolha que tipo de busca que gostaria de executar (por produto, estabelecimento, município ou localização);
2.2.1 Na busca por produto, insira o município desejado e escreva uma palavra-chave ou produto de seu interesse. O aplicativo demonstrará as promoções condizentes com sua busca. Clique no produto escolhido para ter acesso ao painel do estabelecimento e mais promoções válidas;
2.2.2 Na busca pelo estabelecimento, insira o nome de um estabelecimento que o aplicativo buscará e permitirá que ao clicar nele, você acesse seu painel;
2.2.3 Na busca pelo município, insira o município ou uma palavra-chave que a busca demonstrará os estabelecimentos participantes da Mercator, e com acesso para seu painel ao tocar no item desejado;
2.2.4 Na busca por localização, você terá acesso ao mapa com sua localização e os estabelecimentos participantes pontuados no mapa para que possa acessar seu painel com o toque no ícone;
3. Quanto ao painel do estabelecimento, você poderá acessar as promoções disponíveis consultando na lista ou optando por abrir o encarte (se disponível) em imagem para manipulação. Poderá também ver informações pertinentes deste estabelecimento como data e horário de funcionamento, por exemplo.

OBS: _O aplicativo tem funcionamento apenas com o acesso do estabelecimento "Mercado 1", sendo ambos exemplos e apenas este com informações. O acesso em outro estabelecimento induz o aplicativo ao erro, fazendo com que o encerre._

## Confecção e Tecnologias Utilizadas
O aplicativo foi construído em linguagem Java na IDE Android Studio.

No projeto, foram utilizadas ferramentas do Google Firebase em toda a aplicação. Na autenticação do usuário, foi utilizado o Authentication por e-mail para acesso e cadastro. As RecyclerViews de todo o aplicativo foram integradas por meio do Realtime Database, bem como os elementos no mapa com geolocalização e as URLs das imagens de produtos e logos armazenados no Firebase Storage.

O aplicativo também contou com a integração entre dados de latitude e longitude da localização dos estabelecimentos e a Google Maps API. O mapa em si foi personalizado com coloração azul marinho, e utilizado para o usuário verificar estabelecimentos mais próximos a ele conforme sua localidade.

Para a análise de erros e remediação, a ferramenta Firebase Crashlytics foi implementada no aplicativo para a verificação de possíveis erros que venham a ocorrer, além de gerar um relatório estatístico com a frequência dos erros.

## Autor

*  Fabricio Ramos da Costa (http://gitlab.com/ramosaegir)