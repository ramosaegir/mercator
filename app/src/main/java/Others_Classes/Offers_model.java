package Others_Classes;

public class Offers_model {

    private String Imagem, Nome, Preco, DataInicio, DataFinal;

    public Offers_model(){}

    public Offers_model(String imagem, String nome, String preco, String dataInicio, String dataFinal) {
        Imagem = imagem;
        Nome = nome;
        Preco = preco;
        DataInicio = dataInicio;
        DataFinal = dataFinal;
    }

    public String getImagem() {
        return Imagem;
    }

    public void setImagem(String imagem) {
        Imagem = imagem;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public String getPreco() {
        return Preco;
    }

    public void setPreco(String preco) {
        Preco = preco;
    }

    public String getDataInicio() {
        return DataInicio;
    }

    public void setDataInicio(String dataInicio) {
        DataInicio = dataInicio;
    }

    public String getDataFinal() {
        return DataFinal;
    }

    public void setDataFinal(String dataFinal) {
        DataFinal = dataFinal;
    }
}
