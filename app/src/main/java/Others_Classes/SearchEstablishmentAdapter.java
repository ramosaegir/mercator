package Others_Classes;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import aegir.ramos.fabricio.mercator_beta.ProfileActivity;
import aegir.ramos.fabricio.mercator_beta.R;

public class SearchEstablishmentAdapter extends RecyclerView.Adapter<SearchEstablishmentAdapter.SearchEstablishmentViewHolder> {
    Context context;
    ArrayList<String> CODList;
    ArrayList<String> EstabelecimentoList;
    ArrayList<String> ImagemList;
    ArrayList<String> LocalList;
    ArrayList<String> NumPerfilList;

    class SearchEstablishmentViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imagem_img;
        TextView cod_txt, local_txt, estabelecimento_txt, NumPerfil;
        private ItemClickListener itemClickListener;

        public SearchEstablishmentViewHolder(View itemView) {
            super(itemView);
            imagem_img = (ImageView) itemView.findViewById(R.id.imagem_img);
            cod_txt = (TextView) itemView.findViewById(R.id.cod_txt);
            local_txt = (TextView) itemView.findViewById(R.id.local_txt);
            estabelecimento_txt = (TextView) itemView.findViewById(R.id.estabelecimento_txt);
            NumPerfil = (TextView) itemView.findViewById(R.id.NumPerfil);

            itemView.setOnClickListener(this);
        }

        public void setItemClickListener(ItemClickListener itemClickListener){
            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClick(view,getAdapterPosition());
        }
    }

    public SearchEstablishmentAdapter(Context context, ArrayList<String> CODList, ArrayList<String> estabelecimentoList, ArrayList<String> imagemList, ArrayList<String> localList, ArrayList<String> NumPerfilList) {
        this.context = context;
        this.CODList = CODList;
        this.EstabelecimentoList = estabelecimentoList;
        this.ImagemList = imagemList;
        this.LocalList = localList;
        this.NumPerfilList = NumPerfilList;
    }

    @Override
    public SearchEstablishmentAdapter.SearchEstablishmentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.searchestablishment_row, parent, false);
        return new SearchEstablishmentAdapter.SearchEstablishmentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SearchEstablishmentAdapter.SearchEstablishmentViewHolder holder, int position) {

        holder.cod_txt.setText(CODList.get(position));
        holder.local_txt.setText(LocalList.get(position));
        holder.estabelecimento_txt.setText(EstabelecimentoList.get(position));
        Picasso.with(context).load(ImagemList.get(position)).into(holder.imagem_img);
        holder.NumPerfil.setText(NumPerfilList.get(position));

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent intent = new Intent(context, ProfileActivity.class);
                intent.putExtra("STRING_NEEDED",CODList.get(position));
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return EstabelecimentoList.size();
    }


}
