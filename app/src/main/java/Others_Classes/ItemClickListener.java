package Others_Classes;

import android.view.View;

public interface ItemClickListener {
    void onClick(View view, int position);
}
