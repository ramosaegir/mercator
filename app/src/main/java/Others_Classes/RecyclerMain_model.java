package Others_Classes;

public class RecyclerMain_model {

    private String CapaImagem, Nome, DataPost, COD;

    public RecyclerMain_model(){
    }

    public RecyclerMain_model(String capaImagem, String nome, String dataPost, String cod){
        CapaImagem = capaImagem;
        Nome = nome;
        DataPost = dataPost;
        COD = cod;
    }

    public String getCapaImagem() {
        return CapaImagem;
    }

    public void setCapaImagem(String capaImagem) {
        CapaImagem = capaImagem;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public String getDataPost() {
        return DataPost;
    }

    public void setDataPost(String dataPost) {
        DataPost = dataPost;
    }

    public String getCOD() {
        return COD;
    }

    public void setCOD(String COD) {
        this.COD = COD;
    }

}
