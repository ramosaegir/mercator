package Others_Classes;

public class EstabelecimentosMain_model {

    private String Nome, Imagem, COD, NumPerfil;

    public EstabelecimentosMain_model(){
    }

    public EstabelecimentosMain_model(String nome, String imagem, String cod, String numPerfil) {
        Nome = nome;
        Imagem = imagem;
        COD = cod;
        NumPerfil = numPerfil;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public String getImagem() {
        return Imagem;
    }

    public void setImagem(String imagem) {
        Imagem = imagem;
    }

    public String getCOD() {
        return COD;
    }

    public void setCOD(String COD) {
        this.COD = COD;
    }

    public String getNumPerfil() {
        return NumPerfil;
    }

    public void setNumPerfil(String numPerfil) {
        NumPerfil = numPerfil;
    }
}
