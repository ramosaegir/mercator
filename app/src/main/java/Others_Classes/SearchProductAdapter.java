package Others_Classes;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import aegir.ramos.fabricio.mercator_beta.ProfileActivity;
import aegir.ramos.fabricio.mercator_beta.R;

public class SearchProductAdapter extends RecyclerView.Adapter<SearchProductAdapter.SearchProductViewHolder> {
    Context context;
    ArrayList<String> NomeList;
    ArrayList<String> CODList;
    ArrayList<String> EstabelecimentoList;
    ArrayList<String> ImagemList;
    ArrayList<String> PrecoList;
    ArrayList<String> DataInicioList;
    ArrayList<String> DataFinalList;

    class SearchProductViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView imagem_img;
        TextView nome_txt, cod_txt, preco_txt, estabelecimento_txt, DataInicioFinal;
        private ItemClickListener itemClickListener;

        public SearchProductViewHolder(View itemView) {
            super(itemView);
            imagem_img = (ImageView) itemView.findViewById(R.id.imagem_img);
            nome_txt = (TextView) itemView.findViewById(R.id.nome_txt);
            cod_txt = (TextView) itemView.findViewById(R.id.cod_txt);
            preco_txt = (TextView) itemView.findViewById(R.id.preco_txt);
            estabelecimento_txt = (TextView) itemView.findViewById(R.id.estabelecimento_txt);
            DataInicioFinal = (TextView) itemView.findViewById(R.id.DataInicioFinal);
            itemView.setOnClickListener(this);
        }

        public void setItemClickListener(ItemClickListener itemClickListener){
            this.itemClickListener = itemClickListener;
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onClick(view,getAdapterPosition());
        }
    }

    public SearchProductAdapter(Context context, ArrayList<String> nomeList, ArrayList<String> CODList,
                                ArrayList<String> estabelecimentoList, ArrayList<String> imagemList,
                                ArrayList<String> precoList,
                                ArrayList<String> dataInicioList, ArrayList<String> dataFinalList) {
        this.context = context;
        this.NomeList = nomeList;
        this.CODList = CODList;
        this.EstabelecimentoList = estabelecimentoList;
        this.ImagemList = imagemList;
        this.PrecoList = precoList;
        this.DataInicioList = dataInicioList;
        this.DataFinalList = dataFinalList;
    }

    @Override
    public SearchProductAdapter.SearchProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.searchproduct_row, parent, false);
        return new SearchProductAdapter.SearchProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SearchProductViewHolder holder, int position) {
        holder.nome_txt.setText(NomeList.get(position));
        holder.cod_txt.setText(CODList.get(position));
        holder.preco_txt.setText(PrecoList.get(position));
        holder.estabelecimento_txt.setText(EstabelecimentoList.get(position));
        Picasso.with(context).load(ImagemList.get(position)).into(holder.imagem_img);
        holder.DataInicioFinal.setText(DataInicioList.get(position)+" - "+DataFinalList.get(position));

        holder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent intent = new Intent(context, ProfileActivity.class);
                intent.putExtra("STRING_NEEDED",CODList.get(position));
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return NomeList.size();
    }


}
