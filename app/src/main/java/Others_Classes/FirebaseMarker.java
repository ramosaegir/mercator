package Others_Classes;

public class FirebaseMarker {

    public String Nome;
    public String COD;
    public double latitude;
    public double longitude;

    //required empty constructor
    public FirebaseMarker() {
    }

    public FirebaseMarker(String Nome, String COD, double latitude, double longitude) {
        this.Nome = Nome;
        this.COD = COD;
        this.latitude = latitude;
        this.longitude = longitude;
    }

}
