package Others_Classes;

public class Encartes_model {

    private String Data, Imagem, Nome, Imagem2;

    public Encartes_model(){}

    public Encartes_model(String data, String imagem, String nome, String imagem2){
        Data = data;
        Imagem = imagem;
        Imagem2 = imagem2;
        Nome = nome;
    }

    public String getData() {
        return Data;
    }

    public void setData(String data) {
        Data = data;
    }

    public String getImagem() {
        return Imagem;
    }

    public void setImagem(String imagem) {
        Imagem = imagem;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String nome) {
        Nome = nome;
    }

    public String getImagem2() {
        return Imagem2;
    }

    public void setImagem2(String imagem2) {
        Imagem2 = imagem2;
    }
}
