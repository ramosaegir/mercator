package aegir.ramos.fabricio.mercator_beta;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

public class SliderActivity extends AppIntro {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addSlide(AppIntroFragment.newInstance("Bem-vindo(a) à plataforma Mercator!",
                "A Mercator foi criada pensando em ti, que busca a aquisição do melhor produto com o menor preço de forma simples e inovadora. Além disso, o nosso objetivo é assegurar que a necessidade do consumidor seja atendida com eficiência e praticidade.",
                R.drawable.welcome_icon, getColor(R.color.corDaBackground)));
        addSlide(AppIntroFragment.newInstance("Estabelecimentos da Plataforma",
                "Ao pensar com cuidado na necessidade do dia-a-dia do público, a plataforma traz as promoções de mercados, supermercados, hortifrutis, farmácias e drogarias de maneira arrojada e descomplicada."
                , R.drawable.bag_icon, getColor(R.color.corDaBackground)));
        addSlide(AppIntroFragment.newInstance("Funcionalidades da Plataforma",
                "Na plataforma você terá as promoções mais atuais dos estabelecimentos, além de poder executar buscas pelo nome do produto, estabelecimento, município e localização. A plataforma estará em constante aperfeiçoamento para novas funções!"
                , R.drawable.gears_icon, getColor(R.color.corDaBackground)));
        addSlide(AppIntroFragment.newInstance("Permissões da Plataforma",
                "Para que você possa utilizar de forma íntegra a plafatorma, solicitamos que nos forneça permissões de localização e armazenamento."
                , R.drawable.information_icon, getColor(R.color.corDaBackground)));
        addSlide(AppIntroFragment.newInstance("Pronto para Utilização!",
                "Pronto! A plataforma Mercator agradece e te deseja um ótimo uso da ferramenta! Aceite a política de privacidade para proceder!"
                , R.drawable.check_icon, getColor(R.color.corDaBackground)));


        showSkipButton(false);
        askForPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},4);
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        // Do something when users tap on Skip button.
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        // Do something when users tap on Done button.
        Intent intent1 = new Intent(this, PrivacyPolicyActivity.class);
        startActivity(intent1);
        finish();
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
        // Do something when the slide changes.
    }
}
