package aegir.ramos.fabricio.mercator_beta;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.firebase.analytics.FirebaseAnalytics;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import Others_Classes.EstabelecimentosMain_model;

public class SelectionActivity extends AppCompatActivity {

    private RecyclerView RecyclerEstabelecimentos;
    private DatabaseReference mDatabase1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if(bundle != null){
            String cod = (String) bundle.get("STRING_NEEDED1");
            mDatabase1 = FirebaseDatabase.getInstance().getReferenceFromUrl("https://mercator-beta.firebaseio.com/RecyclerMain/"+cod+"/Estabelecimentos");
        }

        RecyclerEstabelecimentos = (RecyclerView) findViewById(R.id.RecyclerEstabelecimentos);
        RecyclerEstabelecimentos.setHasFixedSize(true);
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        RecyclerEstabelecimentos.setLayoutManager(layoutManager);
    }

    @Override
    public void onStart() {
        super.onStart();

        FirebaseRecyclerAdapter<EstabelecimentosMain_model, EstabelecimentosMainViewHolder> firebaseRecyclerAdapterEstabelecimentosMain = new FirebaseRecyclerAdapter<EstabelecimentosMain_model, EstabelecimentosMainViewHolder>(

                EstabelecimentosMain_model.class,
                R.layout.estabelecimentosmain_row,
                EstabelecimentosMainViewHolder.class,
                mDatabase1

        ) {
            @Override
            protected void populateViewHolder(EstabelecimentosMainViewHolder viewHolder, EstabelecimentosMain_model model, final int position) {
                viewHolder.setNome(model.getNome());
                viewHolder.setImagem(getApplicationContext(), model.getImagem());
                viewHolder.setCOD(model.getCOD());
                viewHolder.setNumPerfil(model.getNumPerfil());

                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String cod = ((TextView) RecyclerEstabelecimentos.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.COD)).getText().toString();
                        Intent intent = new Intent(SelectionActivity.this, ProfileActivity.class);
                        intent.putExtra("STRING_NEEDED",cod);
                        startActivity(intent);
                    }
                });
            }


        };

        RecyclerEstabelecimentos.setAdapter(firebaseRecyclerAdapterEstabelecimentosMain);
    }

    @Override
    public void onResume() {
        super.onResume();

        RecyclerEstabelecimentos = (RecyclerView) findViewById(R.id.RecyclerEstabelecimentos);
        RecyclerEstabelecimentos.setHasFixedSize(true);
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        RecyclerEstabelecimentos.setLayoutManager(layoutManager);

        FirebaseRecyclerAdapter<EstabelecimentosMain_model, EstabelecimentosMainViewHolder> firebaseRecyclerAdapterEstabelecimentosMain = new FirebaseRecyclerAdapter<EstabelecimentosMain_model, EstabelecimentosMainViewHolder>(

                EstabelecimentosMain_model.class,
                R.layout.estabelecimentosmain_row,
                EstabelecimentosMainViewHolder.class,
                mDatabase1

        ) {
            @Override
            protected void populateViewHolder(EstabelecimentosMainViewHolder viewHolder, EstabelecimentosMain_model model, final int position) {
                viewHolder.setNome(model.getNome());
                viewHolder.setImagem(getApplicationContext(), model.getImagem());
                viewHolder.setCOD(model.getCOD());
                viewHolder.setNumPerfil(model.getNumPerfil());

                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String cod = ((TextView) RecyclerEstabelecimentos.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.COD)).getText().toString();
                        Intent intent = new Intent(SelectionActivity.this, ProfileActivity.class);
                        intent.putExtra("STRING_NEEDED",cod);
                        startActivity(intent);
                    }
                });
            }


        };

        RecyclerEstabelecimentos.setAdapter(firebaseRecyclerAdapterEstabelecimentosMain);
        firebaseRecyclerAdapterEstabelecimentosMain.notifyDataSetChanged();

    }

    public static class EstabelecimentosMainViewHolder extends RecyclerView.ViewHolder {

        View mView;

        public EstabelecimentosMainViewHolder(View itemView) {
            super(itemView);

            mView = itemView;
        }

        public void setNome(String nome){
            TextView Nome = (TextView) mView.findViewById(R.id.Nome);
            Nome.setText(nome);
        }
        public void setImagem(Context ctx, String imagem){
            ImageView Imagem = (ImageView) mView.findViewById(R.id.Imagem);
            Picasso.with(ctx).load(imagem).into(Imagem);
        }
        public void setCOD(String cod){
            TextView COD = (TextView) mView.findViewById(R.id.COD);
            COD.setText(cod);
        }
        public void setNumPerfil(String numPerfil) {
            TextView NumPerfil = (TextView) mView.findViewById(R.id.NumPerfil);
            NumPerfil.setText(numPerfil);
        }
    }



}
