package aegir.ramos.fabricio.mercator_beta;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

public class PrivacyPolicy2Activity extends AppCompatActivity {

    private android.webkit.WebView WebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy2);

        WebView = (WebView) findViewById(R.id.webview2);
        WebView.loadUrl("https://docs.google.com/document/d/e/2PACX-1vSNEF6tvPYeCnPVbGjVruTUSBT-SEXZr3nyIsiu5RTCviF1mr-an89rWXskNv2hsdboffmURQGYq-e6/pub");


    }
}
