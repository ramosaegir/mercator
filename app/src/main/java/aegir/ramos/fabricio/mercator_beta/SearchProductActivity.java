package aegir.ramos.fabricio.mercator_beta;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import Others_Classes.SearchProductAdapter;

public class SearchProductActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    EditText edit_txt;
    RecyclerView recyclerview;
    DatabaseReference databaseReference;
    DatabaseReference spinnerReference;
    FirebaseUser firebaseUser;
    ArrayList<String> NomeList;
    ArrayList<String> CODList;
    ArrayList<String> EstabelecimentoList;
    ArrayList<String> ImagemList;
    ArrayList<String> PrecoList;
    ArrayList<String> DataInicioList;
    ArrayList<String> DataFinalList;
    SearchProductAdapter searchProductAdapter;
    Spinner spinner;
    String spinnerSelectionRC;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_product);

        edit_txt = (EditText) findViewById(R.id.edit_txt);
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);
        spinner = (Spinner) findViewById(R.id.spinner);

        databaseReference = FirebaseDatabase.getInstance().getReferenceFromUrl("https://mercator-beta.firebaseio.com/RecyclerProdutos");
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        spinnerReference = FirebaseDatabase.getInstance().getReferenceFromUrl("https://mercator-beta.firebaseio.com/RecyclerProdutos");

        recyclerview.setHasFixedSize(true);
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        recyclerview.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        spinnerReference.child("SpinnerMunicipios").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final List<String> municipiosSpinner = new ArrayList<String>();
                for (DataSnapshot spinnerSnapshot: dataSnapshot.getChildren()) {
                    String Municipio = spinnerSnapshot.child("Municipio").getValue(String.class);
                    municipiosSpinner.add(Municipio);
                }

                ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(SearchProductActivity.this, R.layout.spinner_item, municipiosSpinner);
                spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(spinnerAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



        NomeList = new ArrayList<>();
        CODList = new ArrayList<>();
        EstabelecimentoList = new ArrayList<>();
        ImagemList = new ArrayList<>();
        PrecoList = new ArrayList<>();
        DataInicioList = new ArrayList<>();
        DataFinalList = new ArrayList<>();

        spinner.setOnItemSelectedListener(this);

        edit_txt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty()) {
                    setAdapter(s.toString());
                } else {
                    /*
                    * Clear the list when editText is empty
                    * */
                    NomeList.clear();
                    CODList.clear();
                    EstabelecimentoList.clear();
                    ImagemList.clear();
                    PrecoList.clear();
                    DataInicioList.clear();
                    DataFinalList.clear();
                    recyclerview.removeAllViews();

                }
            }
        });



    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        i = spinner.getSelectedItemPosition();
        if(i == 0){
            edit_txt.setVisibility(View.INVISIBLE);
            NomeList.clear();
            CODList.clear();
            EstabelecimentoList.clear();
            ImagemList.clear();
            PrecoList.clear();
            DataInicioList.clear();
            DataFinalList.clear();
            recyclerview.removeAllViews();
        }else{
            edit_txt.setVisibility(View.VISIBLE);
            NomeList.clear();
            CODList.clear();
            EstabelecimentoList.clear();
            ImagemList.clear();
            PrecoList.clear();
            DataInicioList.clear();
            DataFinalList.clear();
            recyclerview.removeAllViews();
            spinnerSelectionRC = String.valueOf(i);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }


    private void setAdapter(final String searchedString) {
        databaseReference.child(spinnerSelectionRC).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                /*
                * Clear the list for every new search
                * */
                NomeList.clear();
                CODList.clear();
                EstabelecimentoList.clear();
                ImagemList.clear();
                PrecoList.clear();
                DataInicioList.clear();
                DataFinalList.clear();
                recyclerview.removeAllViews();


                int counter = 0;

                /*
                * Search all usersfor for matching searched string
                * */
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    String uid = snapshot.getKey();
                    String nome = snapshot.child("Nome").getValue(String.class);
                    String cod = snapshot.child("COD").getValue(String.class);
                    String estabelecimento = snapshot.child("Estabelecimento").getValue(String.class);
                    String imagem = snapshot.child("Imagem").getValue(String.class);
                    String preco = snapshot.child("Preco").getValue(String.class);
                    String datainicio = snapshot.child("DataInicio").getValue(String.class);
                    String datafinal = snapshot.child("DataFinal").getValue(String.class);

                    if (nome.toLowerCase().contains(searchedString.toLowerCase())) {
                        NomeList.add(nome);
                        CODList.add(cod);
                        EstabelecimentoList.add(estabelecimento);
                        ImagemList.add(imagem);
                        PrecoList.add(preco);
                        DataInicioList.add(datainicio);
                        DataFinalList.add(datafinal);
                        counter++;
                    }


                }

                searchProductAdapter = new SearchProductAdapter(SearchProductActivity.this,
                        NomeList, CODList, EstabelecimentoList, ImagemList,
                        PrecoList, DataInicioList, DataFinalList);
                recyclerview.setAdapter(searchProductAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


}
