package aegir.ramos.fabricio.mercator_beta;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import static android.widget.Toast.LENGTH_LONG;

public class ForgetPassActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_pass);
        getWindow().setBackgroundDrawableResource(R.drawable.mercator_login);

        progressDialog = new ProgressDialog(this);
        mAuth = FirebaseAuth.getInstance();

        Button confirma_reset = (Button) findViewById(R.id.confirma_reset);
        final EditText userEmail = (EditText) findViewById(R.id.userEmail);


        confirma_reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    resetPassword();
            }
        });

    }

    private void resetPassword() {

        final EditText userEmail = (EditText) findViewById(R.id.userEmail);
        String Email = userEmail.getText().toString().trim();

        if(TextUtils.isEmpty(Email)) {
            alert("Campo não preenchido corretamente.");
            return;
        }

        progressDialog.setMessage("Enviando e-mail de revalidação...");
        progressDialog.show();

        mAuth
                .sendPasswordResetEmail(userEmail.getText().toString())
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        if(task.isSuccessful()) {
                            alert("Recuperação de acesso iniciada.");
                            finish();
                        }else{
                            alert("Falha na recuperação. Tente novamente.");
                            progressDialog.cancel();
                        }
                    }
                });
    }

    private void alert(String s){
        Toast.makeText(this,s,LENGTH_LONG).show();
    }

}
