package aegir.ramos.fabricio.mercator_beta;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import Others_Classes.Offers_model;

public class ProfileActivity extends AppCompatActivity {

    private FloatingActionButton FAB;
    private TextView COD_Receiver;
    private TextView Nome_Estabelecimento;
    private ImageView Image_logo;
    private Button button3;
    private Button button4;
    private ScrollView Scrollviewinfo;
    private ScrollView ScrollviewSpot;
    private Button FecharInfo_btn;
    private Button FecharSpot_btn;
    private DatabaseReference mDatabase1;
    private DatabaseReference mDatabase2;
    private DatabaseReference mDatabase3;
    private DatabaseReference mDatabase5;
    private DatabaseReference mDatabase6;
    private DatabaseReference mDatabase7;
    private DatabaseReference mDatabase8;
    private DatabaseReference mDatabase9;
    private DatabaseReference mDatabase10;
    private DatabaseReference mDatabaseCounter;
    private DatabaseReference mDatabase11;
    private TextView NumPerfil;
    private TextView Descricao;
    private TextView Atualizacao;
    private TextView Ingresso;
    private TextView Contato;
    private TextView DataHorarioFuncionamento;
    private TextView Enderecos;
    private RecyclerView Recycler_Offers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        // DECLARAÇÃO DAS VARIÁVEIS
        FAB = (FloatingActionButton) findViewById(R.id.FAB);
        COD_Receiver = (TextView) findViewById(R.id.COD_Receiver);
        Nome_Estabelecimento = (TextView) findViewById(R.id.Nome_Estabelecimento);
        Image_logo = (ImageView) findViewById(R.id.Image_logo);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);
        Scrollviewinfo = (ScrollView) findViewById(R.id.Scrollviewinfo);
        ScrollviewSpot = (ScrollView) findViewById(R.id.ScrollviewSpot);
        FecharInfo_btn = (Button) findViewById(R.id.FecharInfo_btn);
        FecharSpot_btn = (Button) findViewById(R.id.FecharSpot_btn);
        Descricao = (TextView) findViewById(R.id.Descricao);
        Atualizacao = (TextView) findViewById(R.id.Atualizacao);
        Ingresso = (TextView) findViewById(R.id.Ingresso);
        Contato = (TextView) findViewById(R.id.Contato);
        DataHorarioFuncionamento = (TextView) findViewById(R.id.DataHorarioFuncionamento);
        Enderecos = (TextView) findViewById(R.id.Enderecos);
        NumPerfil = (TextView) findViewById(R.id.NumPerfil);

        // FIM DA DECLARAÇÃO DAS VARIÁVEIS

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Recycler_Offers.setVisibility(View.GONE);
                Scrollviewinfo.setVisibility(View.VISIBLE);
                FecharInfo_btn.setVisibility(View.VISIBLE);
                ScrollviewSpot.setVisibility(View.GONE);
                FecharSpot_btn.setVisibility(View.GONE);
            }
        });

        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Recycler_Offers.setVisibility(View.GONE);
                Scrollviewinfo.setVisibility(View.GONE);
                FecharInfo_btn.setVisibility(View.GONE);
                ScrollviewSpot.setVisibility(View.VISIBLE);
                FecharSpot_btn.setVisibility(View.VISIBLE);
            }
        });

        FecharInfo_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Recycler_Offers.setVisibility(View.VISIBLE);
                Scrollviewinfo.setVisibility(View.GONE);
                FecharInfo_btn.setVisibility(View.GONE);
                ScrollviewSpot.setVisibility(View.GONE);
                FecharSpot_btn.setVisibility(View.GONE);
            }
        });

        FecharSpot_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Recycler_Offers.setVisibility(View.VISIBLE);
                Scrollviewinfo.setVisibility(View.GONE);
                FecharInfo_btn.setVisibility(View.GONE);
                ScrollviewSpot.setVisibility(View.GONE);
                FecharSpot_btn.setVisibility(View.GONE);
            }
        });

        // RECEPTOR DE INFO DAS ACTIVITIES
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if(bundle != null){
            String nome_estabelecimento_string = (String) bundle.get("STRING_NEEDED");
            COD_Receiver.setText(nome_estabelecimento_string);
            mDatabase1 = FirebaseDatabase.getInstance().getReferenceFromUrl("https://mercator-beta.firebaseio.com/Estabelecimentos/"+nome_estabelecimento_string+"/Nome");
            mDatabase2 = FirebaseDatabase.getInstance().getReferenceFromUrl("https://mercator-beta.firebaseio.com/Ofertas/"+nome_estabelecimento_string+"/Blocos");
            mDatabase3 = FirebaseDatabase.getInstance().getReferenceFromUrl("https://mercator-beta.firebaseio.com/Estabelecimentos/"+nome_estabelecimento_string+"/Logo");
            mDatabase5 = FirebaseDatabase.getInstance().getReferenceFromUrl("https://mercator-beta.firebaseio.com/Estabelecimentos/"+nome_estabelecimento_string+"/Info/Atualizacao");
            mDatabase6 = FirebaseDatabase.getInstance().getReferenceFromUrl("https://mercator-beta.firebaseio.com/Estabelecimentos/"+nome_estabelecimento_string+"/Info/Contato");
            mDatabase7 = FirebaseDatabase.getInstance().getReferenceFromUrl("https://mercator-beta.firebaseio.com/Estabelecimentos/"+nome_estabelecimento_string+"/Info/Descricao");
            mDatabase8 = FirebaseDatabase.getInstance().getReferenceFromUrl("https://mercator-beta.firebaseio.com/Estabelecimentos/"+nome_estabelecimento_string+"/Info/Ingresso");
            mDatabase9 = FirebaseDatabase.getInstance().getReferenceFromUrl("https://mercator-beta.firebaseio.com/Estabelecimentos/"+nome_estabelecimento_string+"/Info/DataHorarioFuncionamento");
            mDatabase10 = FirebaseDatabase.getInstance().getReferenceFromUrl("https://mercator-beta.firebaseio.com/Estabelecimentos/"+nome_estabelecimento_string+"/Enderecos");
            mDatabaseCounter = FirebaseDatabase.getInstance().getReferenceFromUrl("https://mercator-beta.firebaseio.com/Contadores/"+nome_estabelecimento_string+"/Contador");
            mDatabase11 = FirebaseDatabase.getInstance().getReferenceFromUrl("https://mercator-beta.firebaseio.com/Estabelecimentos/"+nome_estabelecimento_string+"/NumPerfil");

        }
        // FIM RECEPTOR DE INFO DA TABBED
        mDatabase1.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                Nome_Estabelecimento.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDatabase3.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String image_logo = dataSnapshot.getValue(String.class);
                Image_logo = (ImageView) findViewById(R.id.Image_logo);
                Context context = null;
                Picasso.with(context).load(image_logo).into(Image_logo);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDatabase5.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                Atualizacao.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDatabase6.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                Contato.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDatabase7.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                Descricao.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDatabase8.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                Ingresso.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDatabase9.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                DataHorarioFuncionamento.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mDatabase10.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                Enderecos.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) { }
        });

        mDatabaseCounter.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                int contador = (int) dataSnapshot.getValue(int.class);
                contador = contador + 1;
                mDatabaseCounter.setValue(contador);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) { }
        });

        mDatabase11.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getValue(String.class);
                NumPerfil.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });



        Recycler_Offers = (RecyclerView) findViewById(R.id.Recycler_Offers);
        Recycler_Offers.setHasFixedSize(true);
        StaggeredGridLayoutManager layoutManager1 = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        Recycler_Offers.setLayoutManager(layoutManager1);

        FAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String cod = COD_Receiver.getText().toString();
                Intent intent = new Intent(ProfileActivity.this, DownloadableContentActivity.class);
                intent.putExtra("STRING_NEEDED",cod);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onStart(){
        super.onStart();

        FirebaseRecyclerAdapter<Offers_model,OffersViewHolder> firebaseRecyclerAdapter1 = new FirebaseRecyclerAdapter<Offers_model, OffersViewHolder>(

                Offers_model.class,
                R.layout.offers_row,
                OffersViewHolder.class,
                mDatabase2

        ) {
            @Override
            protected void populateViewHolder(OffersViewHolder viewHolder, Offers_model model, int position) {
                viewHolder.setNome(model.getNome());
                viewHolder.setPreco(model.getPreco());
                viewHolder.setImagem(getApplicationContext(), model.getImagem());
                viewHolder.setDataInicio(model.getDataInicio());
                viewHolder.setDataFinal(model.getDataFinal());
            }
        };

        Recycler_Offers.setAdapter(firebaseRecyclerAdapter1);
    }

    @Override
    public void onResume(){
        super.onResume();

        Recycler_Offers = (RecyclerView) findViewById(R.id.Recycler_Offers);
        Recycler_Offers.setHasFixedSize(true);
        StaggeredGridLayoutManager layoutManager1 = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        Recycler_Offers.setLayoutManager(layoutManager1);

        FirebaseRecyclerAdapter<Offers_model,OffersViewHolder> firebaseRecyclerAdapter1 = new FirebaseRecyclerAdapter<Offers_model, OffersViewHolder>(

                Offers_model.class,
                R.layout.offers_row,
                OffersViewHolder.class,
                mDatabase2

        ) {
            @Override
            protected void populateViewHolder(OffersViewHolder viewHolder, Offers_model model, int position) {
                viewHolder.setNome(model.getNome());
                viewHolder.setPreco(model.getPreco());
                viewHolder.setImagem(getApplicationContext(), model.getImagem());
                viewHolder.setDataInicio(model.getDataInicio());
                viewHolder.setDataFinal(model.getDataFinal());
            }
        };

        Recycler_Offers.setAdapter(firebaseRecyclerAdapter1);
        firebaseRecyclerAdapter1.notifyDataSetChanged();
    }

    public static class OffersViewHolder extends RecyclerView.ViewHolder {

        View mView;

        public OffersViewHolder(View itemView) {
            super(itemView);

            mView = itemView;
        }

        public void setPreco(String preco){
            TextView preco_txt = (TextView) mView.findViewById(R.id.preco_txt);
            preco_txt.setText(preco);
        }
        public void setNome(String nome){
            TextView nome_txt = (TextView) mView.findViewById(R.id.nome_txt);
            nome_txt.setText(nome);
        }
        public void setImagem(Context ctx, String imagem){
            ImageView imagem_img = (ImageView) mView.findViewById(R.id.imagem_img);
            Picasso.with(ctx).load(imagem).into(imagem_img);
        }
        public void setDataInicio(String dataInicio){
            TextView DataInicio = (TextView) mView.findViewById(R.id.DataInicio);
            DataInicio.setText(dataInicio);
        }
        public void setDataFinal(String dataFinal){
            TextView DataFinal = (TextView) mView.findViewById(R.id.DataFinal);
            DataFinal.setText(dataFinal);
        }

    }

}
