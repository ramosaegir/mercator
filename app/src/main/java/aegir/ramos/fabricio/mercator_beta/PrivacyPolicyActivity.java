package aegir.ramos.fabricio.mercator_beta;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class PrivacyPolicyActivity extends AppCompatActivity {

    private CheckBox mCheckBox;
    private Button mButton;
    private WebView WebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);

        mCheckBox = (CheckBox) findViewById(R.id.mCheckBox);
        mButton = (Button) findViewById(R.id.mButton);
        WebView = (WebView) findViewById(R.id.webview);

        Boolean isPPAccepted = getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                .getBoolean("isPPAccepted", false);

        if(isPPAccepted == true){
            Intent intent = new Intent(this, LoginScreen.class);
            startActivity(intent);
            finish();
        }

        WebView.loadUrl("https://docs.google.com/document/d/e/2PACX-1vSNEF6tvPYeCnPVbGjVruTUSBT-SEXZr3nyIsiu5RTCviF1mr-an89rWXskNv2hsdboffmURQGYq-e6/pub");

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mCheckBox.isChecked()){
                    getSharedPreferences("PREFERENCE", MODE_PRIVATE).edit()
                            .putBoolean("isPPAccepted", true).commit();
                    Intent intent = new Intent(PrivacyPolicyActivity.this, LoginScreen.class);
                    startActivity(intent);
                    finish();
                }else{
                    AlertDialog.Builder builder = new AlertDialog.Builder(PrivacyPolicyActivity.this);
                    builder.setTitle("Acesso Não Permitido");
                    builder.setMessage("Para prosseguir, é necessário que leia e aceite a Política de Privacidade.");

                    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) { }
                    });
                    builder.show();
                }
            }
        });


    }
}
