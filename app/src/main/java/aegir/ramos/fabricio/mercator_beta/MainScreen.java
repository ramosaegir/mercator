package aegir.ramos.fabricio.mercator_beta;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.lang.String;
import java.util.Timer;
import java.util.TimerTask;

import android.support.design.internal.NavigationMenu;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import Others_Classes.RecyclerMain_model;
import Others_Classes.SliderAdapter;
import hotchemi.android.rate.AppRate;
import io.github.yavski.fabspeeddial.FabSpeedDial;

import static android.content.ContentValues.TAG;
import static java.lang.String.valueOf;

public class MainScreen extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference mDatabase4;
    private RecyclerView RecyclerMain;

    public String[] imageUrls;
    public SliderAdapter adapter;
    ViewPager viewPager;
    TabLayout indicator;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(!isConnected(MainScreen.this)) buildDialog(MainScreen.this).show();
        else {
            setContentView(R.layout.activity_main);
        }

        setupFirebaseListener();

        AppRate.with(this)
                .setInstallDays(8)
                .setLaunchTimes(10)
                .setRemindInterval(4)
                .monitor();

        AppRate.showRateDialogIfMeetsConditions(this);
        AppRate.with(this).clearAgreeShowDialog();

        FabSpeedDial fabSpeedDial = (FabSpeedDial) findViewById(R.id.fabSpeedDial);
        fabSpeedDial.setMenuListener(new FabSpeedDial.MenuListener() {
            @Override
            public boolean onPrepareMenu(NavigationMenu navigationMenu) {
                return true; // false: don't show menu

            }

            @Override
            public boolean onMenuItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.action_produto:
                        Intent intent1 = new Intent(MainScreen.this, aegir.ramos.fabricio.mercator_beta.SearchProductActivity.class);
                        startActivity(intent1);
                        return true;
                    case R.id.action_nome:
                        Intent intent2 = new Intent (MainScreen.this, Search_Establishment.class);
                        startActivity(intent2);
                        return true;
                    case R.id.action_municipio:
                        Intent intent3 = new Intent (MainScreen.this, Search_byMunicipio.class);
                        startActivity(intent3);
                        return true;
                    case R.id.action_localizacao:
                        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                        boolean isGPS = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                        boolean isNetwork = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                        if (!isGPS && !isNetwork) {
                            showSettingsAlert();
                        }else{
                            Intent intent4 = new Intent(MainScreen.this, aegir.ramos.fabricio.mercator_beta.MapsActivity.class);
                            startActivity(intent4);
                        }
                        return true;
                }
            return true;
            }

            @Override
            public void onMenuClosed() {

            }
        });


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.



        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "", Snackbar.LENGTH_LONG)
                        .setAction("", null).show();
            }
        });

        RecyclerMain = (RecyclerView) findViewById(R.id.RecyclerMain);
        mDatabase4 = FirebaseDatabase.getInstance().getReference().child("RecyclerMain");
        RecyclerMain.setHasFixedSize(true);
        LinearLayoutManager layoutManager4
                = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        RecyclerMain.setLayoutManager(layoutManager4);


        viewPager=(ViewPager)findViewById(R.id.viewPager);
        indicator=(TabLayout)findViewById(R.id.indicator);

        /// IMAGENS DO SLIDER ATUALIZÁVEIS MANUALMENTE POR AQUI ///
        imageUrls = new String[]{
                "https://firebasestorage.googleapis.com/v0/b/mercator-beta.appspot.com/o/Sliders%2F1.png?alt=media&token=f2a289a1-4e06-4f96-9d06-f23552465c7f",
                "https://firebasestorage.googleapis.com/v0/b/mercator-beta.appspot.com/o/Sliders%2F2.png?alt=media&token=78a9b419-bd3b-4ee1-97ed-40cfab5700e7",
                "https://firebasestorage.googleapis.com/v0/b/mercator-beta.appspot.com/o/Sliders%2F3.png?alt=media&token=48c998ce-e667-4bb7-8910-65de0017018f"
        };

        ViewPager viewPager = findViewById(R.id.viewPager);
        adapter = new SliderAdapter(this, imageUrls);
        viewPager.setAdapter(adapter);
        indicator.setupWithViewPager(viewPager, true);

        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new SliderTimer(), 4000, 6000);
    }


    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting())) return true;
            else return false;
        } else
            return false;
    }

    public AlertDialog.Builder buildDialog(Context c) {

        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setTitle("Sem acesso à internet");
        builder.setMessage("Para a utilização da plataforma, é necessária a conexão com a internet.");

        builder.setPositiveButton("Sair", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                finish();
            }
        });

        return builder;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tabbed__screen, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_exit:
                Toast.makeText(MainScreen.this, "Obrigado pela utilização da plataforma Mercator!", Toast.LENGTH_SHORT).show();
                finish();
                return true;
            case R.id.action_desconectar:
                FirebaseAuth.getInstance().signOut();
                return true;
            case R.id.action_politicaprivacidade:
                Intent intentPP = new Intent(MainScreen.this, PrivacyPolicy2Activity.class);
                startActivity(intentPP);
                return true;
            case R.id.action_remover:
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                alertDialog.setTitle("Exclusão do Usuário");
                alertDialog.setMessage("Tem certeza que deseja a exclusão do seu usuário?");
                alertDialog.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        user.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful()){
                                    Log.d(TAG, "User account deleted");
                                }
                            }
                        });
                    }
                });

                alertDialog.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                alertDialog.show();
                return true;
            case R.id.action_about:
                Intent intent = new Intent(MainScreen.this, AboutActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        FirebaseAuth.getInstance().addAuthStateListener(mAuthListener);

        /// RECYCLER main ADAPTER
        FirebaseRecyclerAdapter<RecyclerMain_model,RecyclerMainViewHolder> firebaseRecyclerAdapterMain = new FirebaseRecyclerAdapter<RecyclerMain_model, RecyclerMainViewHolder>(

                RecyclerMain_model.class,
                R.layout.recyclermain_row,
                RecyclerMainViewHolder.class,
                mDatabase4
        ) {
            @Override
            protected void populateViewHolder(final RecyclerMainViewHolder viewHolder, final RecyclerMain_model model, final int position) {

                viewHolder.setNome(model.getNome());
                viewHolder.setDataPost(model.getDataPost());
                viewHolder.setCOD(model.getCOD());
                viewHolder.setCapaImagem(getApplicationContext(), model.getCapaImagem());

                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String cod2 = ((TextView) RecyclerMain.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.COD)).getText().toString();
                        Intent intent = new Intent(MainScreen.this, SelectionActivity.class);
                        intent.putExtra("STRING_NEEDED1",cod2);
                        startActivity(intent);
                    }
                });

            }
        };
        RecyclerMain.setAdapter(firebaseRecyclerAdapterMain);
        /// FIM RECYCLER main ADAPTER
    }

    @Override
    public void onStop() {
        super.onStop();
        if(mAuthListener != null){
            FirebaseAuth.getInstance().removeAuthStateListener(mAuthListener);
        }
    }

    private void setupFirebaseListener(){
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if(user != null){
                    Log.d(TAG,"onAuthStateChanged: signed_in: " + user.getUid());
                }else{
                    Log.d(TAG, "onAuthStateChanged: signed_out");
                    Toast.makeText(MainScreen.this,"Usuário desconectado com sucesso.", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(MainScreen.this,LoginScreen.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }
        };
    }

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("GPS desativado");
        alertDialog.setMessage("É necessário o uso do GPS para continuar com a busca por localização.\nAtivar GPS?");
        alertDialog.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        alertDialog.setNegativeButton("Não", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alertDialog.show();
    }

    public static class RecyclerMainViewHolder extends RecyclerView.ViewHolder {

        View mView;

        public RecyclerMainViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
        }

        public void setNome(String nome) {
            TextView Nome = (TextView) mView.findViewById(R.id.Nome);
            Nome.setText(nome);
        }

        public void setDataPost(String dataPost){
            TextView DataPost = (TextView) mView.findViewById(R.id.DataPost);
            DataPost.setText(dataPost);
        }

        public void setCOD(String cod) {
            TextView COD = (TextView) mView.findViewById(R.id.COD);
            COD.setText(cod);
        }

        public void setCapaImagem(Context ctx, String capaImagem) {
            ImageView CapaImagem = (ImageView) mView.findViewById(R.id.CapaImagem);
            Picasso.with(ctx).load(capaImagem).into(CapaImagem);
        }
    }

    private class SliderTimer extends TimerTask {

        @Override
        public void run() {
            MainScreen.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (viewPager.getCurrentItem() < imageUrls.length - 1) {
                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                    } else {
                        viewPager.setCurrentItem(0);
                    }
                }
            });
        }
    }

}

