package aegir.ramos.fabricio.mercator_beta;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import Others_Classes.SearchEstablishmentAdapter;

public class Search_Establishment extends AppCompatActivity {

    EditText edit_txt;
    RecyclerView recyclerview;
    DatabaseReference databaseReference;
    FirebaseUser firebaseUser;
    ArrayList<String> CODList;
    ArrayList<String> EstabelecimentoList;
    ArrayList<String> ImagemList;
    ArrayList<String> LocalList;
    ArrayList<String> NumPerfilList;
    SearchEstablishmentAdapter searchEstablishmentAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_establishment);

        edit_txt = (EditText) findViewById(R.id.edit_txt);
        recyclerview = (RecyclerView) findViewById(R.id.recyclerview);

        databaseReference = FirebaseDatabase.getInstance().getReference();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        recyclerview.setHasFixedSize(true);
        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        recyclerview.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        CODList = new ArrayList<>();
        EstabelecimentoList = new ArrayList<>();
        ImagemList = new ArrayList<>();
        LocalList = new ArrayList<>();
        NumPerfilList = new ArrayList<>();

        edit_txt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty()) {
                    setAdapter(s.toString());
                } else {
                    /*
                     * Clear the list when editText is empty
                     * */
                    CODList.clear();
                    EstabelecimentoList.clear();
                    ImagemList.clear();
                    LocalList.clear();
                    NumPerfilList.clear();
                    recyclerview.removeAllViews();

                }
            }
        });


    }

    private void setAdapter(final String searchedString) {
        databaseReference.child("RecyclerEstabelecimentos").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                /*
                 * Clear the list for every new search
                 * */
                CODList.clear();
                EstabelecimentoList.clear();
                ImagemList.clear();
                LocalList.clear();
                NumPerfilList.clear();
                recyclerview.removeAllViews();

                int counter = 0;

                /*
                 * Search all users for matching searched string
                 * */
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    String uid = snapshot.getKey();
                    String cod = snapshot.child("COD").getValue(String.class);
                    String estabelecimento = snapshot.child("Estabelecimento").getValue(String.class);
                    String imagem = snapshot.child("Imagem").getValue(String.class);
                    String local = snapshot.child("Local").getValue(String.class);
                    String numPerfil = snapshot.child("NumPerfil").getValue(String.class);

                    if (estabelecimento.toLowerCase().contains(searchedString.toLowerCase())) {
                        CODList.add(cod);
                        EstabelecimentoList.add(estabelecimento);
                        ImagemList.add(imagem);
                        LocalList.add(local);
                        NumPerfilList.add(numPerfil);
                        counter++;
                    }
                }

                searchEstablishmentAdapter = new SearchEstablishmentAdapter(Search_Establishment.this, CODList, EstabelecimentoList, ImagemList, LocalList, NumPerfilList);
                recyclerview.setAdapter(searchEstablishmentAdapter);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
