package aegir.ramos.fabricio.mercator_beta;

import android.app.ProgressDialog;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.Date;

import static android.widget.Toast.LENGTH_LONG;
import static android.widget.Toast.LENGTH_SHORT;

public class CadastroActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);
        getWindow().setBackgroundDrawableResource(R.drawable.mercator_login);

        progressDialog = new ProgressDialog(this);
        mAuth = FirebaseAuth.getInstance();

        Button confirma_cadastro = (Button) findViewById(R.id.confirma_cadastro);

        confirma_cadastro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerUser();
            }
        });


    }

    private void registerUser() {

        EditText userEmail = (EditText) findViewById(R.id.userEmail);
        EditText userPassword = (EditText) findViewById(R.id.userPassword);
        EditText userPasswordConfirm = (EditText) findViewById(R.id.userPasswordConfirm);

        final String email = userEmail.getText().toString().trim();
        String password = userPassword.getText().toString().trim();
        String passwordconfirm = userPasswordConfirm.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            //email is empty
            alert("Por favor, insira o e-mail.");
            return;
        }
        if (TextUtils.isEmpty(password)) {
            //password is empty
            alert("Por favor, insira a senha.");
            return;
        }
        if (TextUtils.isEmpty(passwordconfirm)) {
            //password is empty
            alert("Por favor, insira a senha de confirmação.");
            return;
        }
        if (!passwordconfirm.equals(password)) {
            //passwords not equals
            alert("Senhas incoerentes, verificar novamente.");
            userPassword.setText("");
            userPasswordConfirm.setText("");
            return;
        }

        progressDialog.setMessage("Cadastrando usuário...");
        progressDialog.show();

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            alert("Cadastro efetuado com sucesso.");
                            finish();
                        } else {
                            alert("Não foi possivel cadastrar o usuário.\nTente novamente.");
                            progressDialog.cancel();
                        }
                    }
                });
    }

    private void alert(String s){
        Toast.makeText(this,s,LENGTH_SHORT).show();
    }

}
