package aegir.ramos.fabricio.mercator_beta;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        final Boolean isSliderFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                .getBoolean("isSliderFirstRun", true);

        Handler handle = new Handler();
        handle.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(isSliderFirstRun) {
                    getSharedPreferences("PREFERENCE", MODE_PRIVATE).edit()
                            .putBoolean("isSliderFirstRun", false).commit();
                    mostrarSlider();
                }else{
                    mostrarPP();
                }
            }
        }, 3000);
    }

    private void mostrarPP() {
        Intent intent = new Intent(SplashScreen.this, PrivacyPolicyActivity.class);
        startActivity(intent);
        finish();
    }

    private void mostrarSlider() {
        Intent intent = new Intent(SplashScreen.this, SliderActivity.class);
        startActivity(intent);
        finish();
    }
}
