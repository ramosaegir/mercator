package aegir.ramos.fabricio.mercator_beta;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.CompoundButton;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static android.widget.Toast.LENGTH_LONG;

public class LoginScreen extends AppCompatActivity implements TextWatcher, CompoundButton.OnCheckedChangeListener {

    private EditText etUsername, etPass;
    private CheckBox rem_userpass;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private static final String PREF_NAME = "prefs";
    private static final String KEY_REMEMBER = "remember";
    private static final String KEY_USERNAME = "username";
    private static final String KEY_PASS = "password";
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);
        getWindow().setBackgroundDrawableResource(R.drawable.mercator_login);

        progressDialog = new ProgressDialog(this);

        mAuth = FirebaseAuth.getInstance();

// SHARED PREFERENCES
        sharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        etUsername = (EditText) findViewById(R.id.tLogin);
        etPass = (EditText) findViewById(R.id.tSenha);
        rem_userpass = (CheckBox) findViewById(R.id.checkbox_salvar);

        if (sharedPreferences.getBoolean(KEY_REMEMBER, false))
            rem_userpass.setChecked(true);
        else
            rem_userpass.setChecked(false);

        etUsername.setText(sharedPreferences.getString(KEY_USERNAME, ""));
        etPass.setText(sharedPreferences.getString(KEY_PASS, ""));

        etUsername.addTextChangedListener(this);
        etPass.addTextChangedListener(this);
        rem_userpass.setOnCheckedChangeListener(this);
// SHARED PREFERENCES

        final Button btlogin = (Button) findViewById(R.id.btLogin);
        final Button btCadastrese = (Button) findViewById(R.id.button2);
        final TextView tEsqueceu_Senha = (TextView) findViewById(R.id.tEsqueceu_Senha);


        btlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TextView tLogin = (TextView) findViewById(R.id.tLogin);
                TextView tSenha = (TextView) findViewById(R.id.tSenha);
                CheckBox checkbox_salvar = (CheckBox) findViewById(R.id.checkbox_salvar);
                String login = tLogin.getText().toString();
                String senha = tSenha.getText().toString();

// REFERENTE AO SIGNIN DO FIREBASE
                String email = tLogin.getText().toString();
                String password = tSenha.getText().toString();
                progressDialog.setMessage("Acessando...");
                progressDialog.show();
                if (TextUtils.isEmpty(email) || TextUtils.isEmpty(password)) {
                    alert("Campos não preenchidos corretamente");
                    progressDialog.cancel();
                } else {
                    mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (!task.isSuccessful()) {
                                alert("Acesso não permitido");
                                progressDialog.cancel();
                            }
                        }
                    });
                }
            }
        });

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                TextView tLogin = (TextView) findViewById(R.id.tLogin);
                String email = tLogin.getText().toString();
                TextView tSenha = (TextView) findViewById(R.id.tSenha);
                String password = tSenha.getText().toString();
                if (firebaseAuth.getCurrentUser() != null) {
                    FirebaseUser user = mAuth.getCurrentUser();
                    mAuth.updateCurrentUser(user);
                    Intent intent = new Intent(LoginScreen.this, MainScreen.class);
                    startActivity(intent);
                    finish();
                }
            }
        };


        btCadastrese.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginScreen.this, CadastroActivity.class);
                startActivity(intent);
            }
        });



        tEsqueceu_Senha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginScreen.this, ForgetPassActivity.class);
                startActivity(intent);
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        managePrefs();
    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        managePrefs();
    }

    private void managePrefs() {
        if (rem_userpass.isChecked()) {
            editor.putString(KEY_USERNAME, etUsername.getText().toString().trim());
            editor.putString(KEY_PASS, etPass.getText().toString().trim());
            editor.putBoolean(KEY_REMEMBER, true);
            editor.apply();
        } else {
            editor.putBoolean(KEY_REMEMBER, false);
            editor.remove(KEY_PASS);//editor.putString(KEY_PASS,"");
            editor.remove(KEY_USERNAME);//editor.putString(KEY_USERNAME, "");
            editor.apply();
        }
    }




    private void alert(String s){
        Toast.makeText(this,s,Toast.LENGTH_SHORT).show();
    }


}

