package aegir.ramos.fabricio.mercator_beta;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.UUID;

import Others_Classes.Encartes_model;
import Others_Classes.SaveImageHelper;
import dmax.dialog.SpotsDialog;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class DownloadableContentActivity extends AppCompatActivity {

    private static final int PERMISSION_REQUEST_CODE = 1000;
    private RecyclerView Recycler_Encarte;
    private DatabaseReference mDatabase1;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode)
        {
            case PERMISSION_REQUEST_CODE:
            {
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    Toast.makeText(this, "Permissão concedida", Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(this,"Permissão Negada", Toast.LENGTH_SHORT).show();
            }
            break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_downloadable_content);

        if(ActivityCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        }

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if(bundle != null){
            String cod = (String) bundle.get("STRING_NEEDED");
            mDatabase1 = FirebaseDatabase.getInstance().getReferenceFromUrl("https://mercator-beta.firebaseio.com/Ofertas/"+cod+"/Encartes");
        }


        Recycler_Encarte = (RecyclerView) findViewById(R.id.Recycler_Encarte);
        Recycler_Encarte.setHasFixedSize(true);
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        Recycler_Encarte.setLayoutManager(layoutManager);
    }

    @Override
    public void onStart(){
        super.onStart();

        FirebaseRecyclerAdapter<Encartes_model, DownloadableContentActivity.EncartesViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Encartes_model, DownloadableContentActivity.EncartesViewHolder>(

                Encartes_model.class,
                R.layout.encartes_row,
                DownloadableContentActivity.EncartesViewHolder.class,
                mDatabase1

        ) {
            @Override
            protected void populateViewHolder(DownloadableContentActivity.EncartesViewHolder viewHolder, Encartes_model model, final int position) {
                viewHolder.setData(model.getData());
                viewHolder.setNome(model.getNome());
                viewHolder.setImagem2(model.getImagem2());
                viewHolder.setImagem(getApplicationContext(), model.getImagem());

                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(ActivityCompat.checkSelfPermission(DownloadableContentActivity.this, WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
                            Toast.makeText(DownloadableContentActivity.this,"Você deve conceder a permissão para prosseguir", Toast.LENGTH_SHORT).show();
                            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                            return;
                        }
                        else{
                            String cod = ((TextView) Recycler_Encarte.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.Imagem2)).getText().toString();
                            AlertDialog dialog = new SpotsDialog(DownloadableContentActivity.this);
                            dialog.show();
                            dialog.setMessage("Efetuando o download...");
                            String fileName = UUID.randomUUID().toString()+".jpg";
                            Picasso.with(getBaseContext())
                                    .load(cod)
                                    .into(new SaveImageHelper(getBaseContext(),
                                            dialog,
                                            getApplicationContext().getContentResolver(),
                                            fileName,
                                            "Descrição da Imagem"));
                            Toast.makeText(DownloadableContentActivity.this,"Imagem salva na pasta Pictures.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
        };

        Recycler_Encarte.setAdapter(firebaseRecyclerAdapter);
    }


    @Override
    public void onResume(){
        super.onResume();

        Recycler_Encarte = (RecyclerView) findViewById(R.id.Recycler_Encarte);
        Recycler_Encarte.setHasFixedSize(true);
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        Recycler_Encarte.setLayoutManager(layoutManager);

        FirebaseRecyclerAdapter<Encartes_model, DownloadableContentActivity.EncartesViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Encartes_model, DownloadableContentActivity.EncartesViewHolder>(

                Encartes_model.class,
                R.layout.encartes_row,
                DownloadableContentActivity.EncartesViewHolder.class,
                mDatabase1

        ) {
            @Override
            protected void populateViewHolder(DownloadableContentActivity.EncartesViewHolder viewHolder, Encartes_model model, final int position) {
                viewHolder.setData(model.getData());
                viewHolder.setNome(model.getNome());
                viewHolder.setImagem2(model.getImagem2());
                viewHolder.setImagem(getApplicationContext(), model.getImagem());

                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(ActivityCompat.checkSelfPermission(DownloadableContentActivity.this, WRITE_EXTERNAL_STORAGE)!= PackageManager.PERMISSION_GRANTED){
                            Toast.makeText(DownloadableContentActivity.this,"Você deve conceder a permissão para prosseguir", Toast.LENGTH_SHORT).show();
                            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                            return;
                        }
                        else{
                            String cod = ((TextView) Recycler_Encarte.findViewHolderForAdapterPosition(position).itemView.findViewById(R.id.Imagem2)).getText().toString();
                            AlertDialog dialog = new SpotsDialog(DownloadableContentActivity.this);
                            dialog.show();
                            dialog.setMessage("Efetuando o download...");
                            String fileName = UUID.randomUUID().toString()+".jpg";
                            Picasso.with(getBaseContext())
                                    .load(cod)
                                    .into(new SaveImageHelper(getBaseContext(),
                                            dialog,
                                            getApplicationContext().getContentResolver(),
                                            fileName,
                                            "Descrição da Imagem"));
                            Toast.makeText(DownloadableContentActivity.this,"Imagem salva na pasta Pictures.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
        };

        Recycler_Encarte.setAdapter(firebaseRecyclerAdapter);
        firebaseRecyclerAdapter.notifyDataSetChanged();
    }


    public static class EncartesViewHolder extends RecyclerView.ViewHolder {

        View mView;

        public EncartesViewHolder(View itemView) {
            super(itemView);

            mView = itemView;
        }

        public void setData(String data){
            TextView data_txt = (TextView) mView.findViewById(R.id.Data);
            data_txt.setText(data);
        }
        public void setNome(String nome){
            TextView nome_txt = (TextView) mView.findViewById(R.id.Nome);
            nome_txt.setText(nome);
        }
        public void setImagem(Context ctx, String imagem){
            ImageView imagem_img = (ImageView) mView.findViewById(R.id.Imagem);
            Picasso.with(ctx).load(imagem).into(imagem_img);
        }
        public void setImagem2(String imagem2){
            TextView imagem2_txt = (TextView) mView.findViewById(R.id.Imagem2);
            imagem2_txt.setText(imagem2);
        }

    }

}
